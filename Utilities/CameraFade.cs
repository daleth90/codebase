﻿using System;
using System.Collections;
using UnityEngine;

public class CameraFade : MonoBehaviour {
    [SerializeField]
    private CanvasGroup canvasGroup = null;

    public static CameraFade Instance { get; private set; }

    private void Awake() {
        Instance = this;
    }

    public void DoFadeInOutProcess( Action callback, float time = 1f ) {
        Debug.Assert( canvasGroup != null, "CanvasGroup is null", this );

        if ( time <= 0f || Mathf.Approximately( time, 0f ) ) {
            Debug.LogWarning( "Time of fading process <= 0, will be set to 1.0 in case of error" );
            time = 1f;
        }

        StopAllCoroutines();
        StartCoroutine( FadeProcess( callback, time ) );
    }

    private IEnumerator FadeProcess( Action callback, float time ) {
        while ( canvasGroup.alpha < 1f ) {
            canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
        }

        if ( callback != null ) {
            callback.Invoke();
        }

        while ( canvasGroup.alpha > 0f ) {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
    }
}
