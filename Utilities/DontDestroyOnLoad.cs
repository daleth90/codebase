﻿using UnityEngine;

namespace DeltaPosition {
    public class DontDestroyOnLoad : MonoBehaviour {
        void Awake() {
            DontDestroyOnLoad( gameObject );
        }
    }
}
