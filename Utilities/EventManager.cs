﻿using System;
using System.Collections.Generic;

public class EventManager {
    private readonly Dictionary<string, List<Action>> events = new Dictionary<string, List<Action>>();

    private static EventManager instance;

    public static EventManager Instance {
        get {
            if ( instance == null ) {
                instance = new EventManager();
            }

            return instance;
        }
    }

    private EventManager() {

    }

    public void StartListening( string eventName, Action action ) {
        if ( action == null ) {
            return;
        }

        if ( events.TryGetValue( eventName, out List<Action> actions ) ) {
            actions.Add( action );
        }
        else {
            actions = new List<Action> { action };
            events.Add( eventName, actions );
        }
    }

    public void StopListening( string eventName, Action action ) {
        if ( events.TryGetValue( eventName, out List<Action> actions ) ) {
            actions.Remove( action );
        }
    }

    public void TriggerEvent( string eventName ) {
        if ( events.TryGetValue( eventName, out List<Action> actions ) ) {
            for ( int i = 0; i < actions.Count; i++ ) {
                actions[ i ].Invoke();
            }
        }
    }
}
