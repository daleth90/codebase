﻿using System.IO;
using UnityEngine;

namespace DeltaPosition {
    public static class FileUtility {
        public static string GetDataPath() {
#if !UNITY_EDITOR && ( UNITY_IOS || UNITY_ANDROID )
            return Application.persistentDataPath;
#else
            DirectoryInfo dirInfo = new DirectoryInfo( Application.dataPath );
            return dirInfo.Parent.FullName;
#endif
        }

        /// <summary>
        /// Load text file with relative path
        /// </summary>
        /// <param name="relativePath">File path with the extension, relative to
        /// Application.persistentDataPath in Android/iOS or Application.dataPath.parent in Windows</param>
        public static string LoadTextFileRelative( string relativePath ) {
            string filePath = Path.Combine( GetDataPath(), relativePath );
            return LoadTextFile( filePath );
        }

        /// <summary>
        /// Load text file
        /// </summary>
        /// <param name="filePath">Absolute path of the file with the extension</param>
        public static string LoadTextFile( string filePath ) {
            using ( FileStream fs = new FileStream( filePath, FileMode.OpenOrCreate ) ) {
                using ( StreamReader reader = new StreamReader( fs ) ) {
                    return reader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Load file as byte array with relative path
        /// </summary>
        /// <param name="relativePath">File path with the extension, relative to
        /// Application.persistentDataPath in Android/iOS or Application.dataPath.parent in Windows</param>
        public static byte[] LoadBinaryFileRelative( string relativePath ) {
            string filePath = Path.Combine( GetDataPath(), relativePath );
            return LoadBinaryFile( filePath );
        }

        /// <summary>
        /// Load file as byte array
        /// </summary>
        /// <param name="filePath">Absolute path of the file with the extension</param>
        public static byte[] LoadBinaryFile( string filePath ) {
            using ( FileStream fs = new FileStream( filePath, FileMode.OpenOrCreate ) ) {
                using ( BinaryReader binaryReader = new BinaryReader( fs ) ) {
                    return binaryReader.ReadBytes( (int)fs.Length );
                }
            }
        }

        /// <summary>
        /// Load PNG/JPG image with relative path
        /// </summary>
        /// <param name="relativePath">File path with the extension, relative to
        /// Application.persistentDataPath in Android/iOS or Application.dataPath.parent in Windows</param>
        public static Texture2D LoadImageRelative( string relativePath ) {
            string filePath = Path.Combine( GetDataPath(), relativePath );
            return LoadImage( filePath );
        }

        /// <summary>
        /// Load PNG/JPG image
        /// </summary>
        /// <param name="filePath">Absolute path of the file with the extension</param>
        public static Texture2D LoadImage( string filePath ) {
            Texture2D texture = new Texture2D( 2, 2 );  // Texture size does not matter, since LoadImage will replace with incoming image size.
            byte[] fileBytes = LoadBinaryFile( filePath );
            if ( !texture.LoadImage( fileBytes ) ) {
                Debug.LogErrorFormat( "LoadTexture Failed! filePath = {0}", filePath );
            }

            return texture;
        }

        /// <summary>
        /// Load a PNG/JPG image to a Sprite with relative path
        /// </summary>
        /// <param name="relativePath">File path with the extension, relative to
        /// Application.persistentDataPath in Android/iOS or Application.dataPath.parent in Windows</param>
        /// <param name="pixelsPerUnit">The number of pixels in the sprite that correspond to one unit in world space</param>
        public static Sprite LoadImageAsSpriteRelative( string relativePath, float pixelsPerUnit = 100.0f ) {
            string filePath = Path.Combine( GetDataPath(), relativePath );
            return LoadImageAsSprite( filePath, pixelsPerUnit );
        }

        /// <summary>
        /// Load a PNG/JPG image to a Sprite
        /// </summary>
        /// <param name="filePath">Absolute path of the file with the extension</param>
        /// <param name="pixelsPerUnit">The number of pixels in the sprite that correspond to one unit in world space</param>
        public static Sprite LoadImageAsSprite( string filePath, float pixelsPerUnit = 100.0f ) {
            Texture2D texture = LoadImage( filePath );
            Sprite sprite = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), new Vector2( 0.5f, 0.5f ), pixelsPerUnit );
            return sprite;
        }
    }
}
