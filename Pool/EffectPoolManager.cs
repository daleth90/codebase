﻿using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    public class EffectPoolManager {
        private readonly Dictionary<GameObject, UnityObjectPool<GameObject>> pools = new Dictionary<GameObject, UnityObjectPool<GameObject>>();

        public GameObject Get( GameObject key ) {
            if ( !pools.ContainsKey( key ) ) {
                CreateNewPool( key );
            }

            return pools[ key ].Get();
        }

        private void CreateNewPool( GameObject key ) {
            UnityObjectPool<GameObject> pool = new UnityObjectPool<GameObject>( key, 5, false );
            pools.Add( key, pool );
        }

        public void Recover( GameObject obj ) {
            foreach ( var pool in pools.Values ) {
                if ( pool.IsObjectBelongToPool( obj ) ) {
                    pool.Recover( obj );
                    return;
                }
            }
        }

        public void Destroy() {
            foreach ( var pool in pools.Values ) {
                pool.Destroy();
            }

            pools.Clear();
        }
    }
}
