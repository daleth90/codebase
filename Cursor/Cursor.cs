﻿using UnityEngine;
using UnityEngine.UI;

namespace DeltaPosition {
    public class Cursor : MonoBehaviour, ICursor {
        [SerializeField]
        private Image cursorImage = null;
        [SerializeField]
        private Vector2 offset = new Vector2( 0f, 0f );

        private void Start() {
            UnityEngine.Cursor.visible = false;
        }

        private void Update() {
            cursorImage.rectTransform.position = new Vector2( Input.mousePosition.x + offset.x, Input.mousePosition.y + offset.y );
        }

        public bool GetButton() {
            return Input.GetMouseButton( 0 );
        }

        public bool GetButtonDown() {
            return Input.GetMouseButtonDown( 0 );
        }

        public bool GetButtonUp() {
            return Input.GetMouseButtonUp( 0 );
        }

        public Vector2 GetCursorPosition() {
            return Input.mousePosition;
        }
    }
}
