﻿using NUnit.Framework;
using System.Collections.Generic;

namespace DeltaPosition {
    public class CollectionExtensionTests {
        [Test]
        public void ShuffleEnumerable_IsEqualToTheOriginalAfterSorting() {
            List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> result = new List<int>( list.Count );

            foreach ( int value in ( list as IEnumerable<int> ).Shuffle() ) {
                result.Add( value );
            }

            result.Sort();
            Assert.AreEqual( list, result );
        }

        [Test]
        public void ShuffleList_IsEqualToTheOriginalAfterSorting() {
            List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> copy = new List<int>( list );

            copy.Shuffle();

            copy.Sort();
            Assert.AreEqual( list, copy );
        }
    }
}
