﻿using UnityEngine;

namespace DeltaPosition {
    public static class UnityEngineExtensions {
        public static void SortChildrenByName( this Transform transform ) {
            // Use Insertion Sort
            for ( int i = 1; i < transform.childCount; i++ ) {
                Transform insertChild = transform.GetChild( i );
                for ( int j = i - 1; j >= 0; j-- ) {
                    Transform comparedChild = transform.GetChild( j );
                    if ( string.Compare( comparedChild.name, insertChild.name ) > 0 ) {
                        insertChild.SetSiblingIndex( j );
                    }
                    else {
                        break;
                    }
                }
            }
        }
    }
}
