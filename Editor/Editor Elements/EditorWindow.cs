﻿namespace DeltaPosition {
    public class EditorWindow : UnityEditor.EditorWindow {
        private bool isFirstFrame;

        private void OnEnable() {
            isFirstFrame = true;
        }

        private void OnDisable() {
            Dispose();
        }

        private void OnGUI() {
            // Escape the problem that some engine resources haven't been initialized yet at OnEnable() after re-compiling 
            if ( isFirstFrame ) {
                isFirstFrame = false;
                Initialize();
            }

            GUIRender();
        }

        protected virtual void Initialize() {

        }

        protected virtual void GUIRender() {

        }

        protected virtual void Dispose() {

        }
    }
}
