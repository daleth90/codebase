﻿using UnityEngine;

namespace DeltaPosition {
    public abstract class EditorArea {
        public virtual void Initialize() {

        }

        public void GUIRender( Rect position ) {
            GUILayout.BeginArea( position );
            RenderContent( position );
            GUILayout.EndArea();
        }

        protected virtual void RenderContent( Rect position ) {

        }

        public virtual void Dispose() {

        }
    }
}
