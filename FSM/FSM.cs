﻿using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    public abstract class FSM<T> where T : class {
        private readonly T instance;
        private readonly State<T> startState;

        private readonly Stack<State<T>> stateStack = new Stack<State<T>>();

        private bool enableLog = false;

        public State<T> CurrentState { get; protected set; }
        public bool IsRunning { get; private set; } = false;
        public float ElapseTime { get; private set; } = 0f;

        public FSM( T instance, State<T> startState ) {
            this.instance = instance;
            this.startState = startState;
        }

        public void SetLogEnable( bool enable ) {
            enableLog = enable;
        }

        public void Start() {
            if ( IsRunning ) {
                return;
            }

            if ( enableLog ) {
                LogFormat( "{0} Enter (Start State)", startState.GetType().Name );
            }

            IsRunning = true;
            CurrentState = startState;
            CurrentState.Enter( instance );
        }

        public void Tick( float deltaTime ) {
            if ( !IsRunning ) {
                return;
            }

            ElapseTime += deltaTime;
            State<T> nextState = CurrentState.Update( instance );
            if ( nextState != null ) {
                LogFormat( "{0} Exit", CurrentState.GetType().Name );
                CurrentState.Exit( instance );

                CurrentState = nextState;

                ElapseTime = 0f;
                LogFormat( "{0} Enter", CurrentState.GetType().Name );
                CurrentState.Enter( instance );
            }
        }

        public void Stop() {
            if ( !IsRunning ) {
                return;
            }

            LogFormat( "{0} Exit (FSM Stop)", CurrentState.GetType().Name );
            CurrentState.Exit( instance );

            CurrentState = null;
            IsRunning = false;
            ElapseTime = 0f;
        }

        public void PushState( State<T> nextState ) {
            stateStack.Push( CurrentState );
            CurrentState = nextState;

            LogFormat( "{0} Enter (Push, Stack {1})", CurrentState.GetType().Name, stateStack.Count );
            CurrentState.Enter( instance );
        }

        public void PopState() {
            LogFormat( "{0} Exit (Pop, Stack {1})", CurrentState.GetType().Name, stateStack.Count );
            CurrentState.Exit( instance );
            CurrentState = stateStack.Pop();
        }

        private void LogFormat( string format, params object[] args ) {
            if ( enableLog ) {
                Debug.LogFormat( format, args );
            }
        }
    }
}
