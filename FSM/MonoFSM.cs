﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    public class MonoFSM<T> where T : class {
        protected readonly T instance;
        private readonly MonoState<T> startState;

        private readonly Stack<MonoState<T>> stateStack = new Stack<MonoState<T>>();

        private AsyncProcessor asyncProcesser;

        public MonoState<T> CurrentState { get; protected set; }
        public StateTransition<T> CurrentTransition { get; protected set; }
        public bool IsRunning { get; private set; } = false;
        public bool IsTransitting { get { return CurrentTransition != null; } }
        private AsyncProcessor AsyncProcesser {
            get {
                if ( asyncProcesser == null ) {
                    var go = new GameObject( string.Format( "AsyncProcesser[{0}]", GetType().Name ) );
                    asyncProcesser = go.AddComponent<AsyncProcessor>();
                }

                return asyncProcesser;
            }
        }

        public MonoFSM( T instance, MonoState<T> startState ) {
            this.instance = instance;
            this.startState = startState;
        }

        public void Start() {
            Debug.LogFormat( "{0} Enter (Start State)", startState.GetType().Name );
            IsRunning = true;
            CurrentState = startState;
            CurrentState.Enter( instance );
        }

        public void Tick() {
            if ( !IsRunning ) {
                return;
            }

            if ( IsTransitting ) {
                return;
            }

            StateTransition<T> transition = CurrentState.Update( instance );
            if ( transition != null ) {
                HandleTransition( transition );
            }
        }

        protected void HandleTransition( StateTransition<T> transition ) {
            var method = transition.TransitionMethod;
            if ( method == null ) {
                AsyncProcesser.StartCoroutine( GoToState( transition.NextState ) );
            }
            else {
                CurrentTransition = transition;
                AsyncProcesser.StartCoroutine( TransitionProcess( transition.NextState, method ) );
            }
        }

        private IEnumerator GoToState( MonoState<T> nextState ) {
            Debug.LogFormat( "{0} Exit", CurrentState.GetType().Name );
            CurrentState.Exit( instance );
            yield return CurrentState.CleanUp( instance );

            CurrentState = nextState;

            Debug.LogFormat( "{0} Enter", CurrentState.GetType().Name );
            yield return CurrentState.SetUp( instance );
            CurrentState.Enter( instance );
        }

        public IEnumerator TransitionProcess( MonoState<T> nextState, ITransitionMethod<T> method ) {
            yield return method.BeforeGoToState( instance );
            yield return GoToState( nextState );
            yield return method.AfterGoToState( instance );
            CurrentTransition = null;
        }

        public void Stop() {
            Debug.LogFormat( "{0} Exit (FSM Stop)", CurrentState.GetType().Name );
            CurrentState.Exit( instance );

            CurrentState = null;
            IsRunning = false;
        }

        public void PushState( MonoState<T> nextState ) {
            stateStack.Push( CurrentState );
            CurrentState = nextState;

            Debug.LogFormat( "{0} Enter (Push, Stack {1})", CurrentState.GetType().Name, stateStack.Count );
            CurrentState.Enter( instance );
        }

        public void PopState() {
            Debug.LogFormat( "{0} Exit (Pop, Stack {1})", CurrentState.GetType().Name, stateStack.Count );
            CurrentState.Exit( instance );
            CurrentState = stateStack.Pop();
        }
    }
}
