﻿using System.Collections;

namespace DeltaPosition {
    public abstract class MonoState<T> where T : class {
        public virtual IEnumerator SetUp( T instance ) {
            yield break;
        }

        public virtual void Enter( T instance ) {

        }

        public virtual StateTransition<T> Update( T instance ) {
            return null;
        }

        public virtual void Exit( T instance ) {

        }

        public virtual IEnumerator CleanUp( T instance ) {
            yield break;
        }
    }
}
